package de.fraunhofer.igd.geo.vgv.flink.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.flink.util.Collector;

public class MockCollector<T> implements Collector<T> {

	private List<T> records = new ArrayList<>();
	
	@Override
	public void collect(T record) {
		records.add(record);
	}

	@Override
	public void close() {
		// do nothing
	}
	
	public int getRecordCount() {
		return records.size();
	}
	
	public T getRecord(int index) {
		return records.get(index);
	}

}
