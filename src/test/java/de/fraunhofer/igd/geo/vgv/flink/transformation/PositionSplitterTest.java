package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Position;
import de.fraunhofer.igd.geo.vgv.flink.util.MockCollector;

public class PositionSplitterTest {
	
	private final PositionSplitter splitter = new PositionSplitter(5L);
	private MockCollector<Tuple2<Long, Position>> collector;
	
	@BeforeEach
	public void setup() {
		collector = new MockCollector<>();
	}
	
	@Test
	public void testProducesRecordPair() throws Exception {
		splitter.flatMap(new Tuple2<Long, Position>(0L, new Position(42, 13)), collector);
		assertEquals(2, collector.getRecordCount());
		Tuple2<Long, Position> result1 = collector.getRecord(0);
		assertEquals(0, result1.f0.longValue());
		assertEquals(42, result1.f1.getLongitude(), 0.00001);
		assertEquals(13, result1.f1.getLatitude(), 0.00001);
		Tuple2<Long, Position> result2 = collector.getRecord(1);
		assertEquals(1, result2.f0.longValue());
		assertEquals(42, result2.f1.getLongitude(), 0.00001);
		assertEquals(13, result2.f1.getLatitude(), 0.00001);
	}
	
	@Test
	public void testLastIndexHasWrapAround() throws Exception {
		splitter.flatMap(new Tuple2<Long, Position>(4L, new Position(42, 13)), collector);
		assertEquals(2, collector.getRecordCount());
		Tuple2<Long, Position> result1 = collector.getRecord(0);
		assertEquals(4, result1.f0.longValue());
		Tuple2<Long, Position> result2 = collector.getRecord(1);
		assertEquals(0, result2.f0.longValue());
	}

}
