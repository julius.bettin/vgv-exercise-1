package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Position;

public class PositionParserTest {
	
	private final PositionParser parser = new PositionParser();
	
	@Test
	public void testParsesValidCoordinateCorrectly() throws Exception {
		Tuple2<Long, Position> result = parser.map("0 8.417499542236328 55.05652618408209");
		assertEquals(0L, result.f0.longValue());
		assertEquals(8.417499542236328, result.f1.getLongitude(), 0.000001);
		assertEquals(55.05652618408209, result.f1.getLatitude(), 0.000001);
	}
	
	@Test
	public void testThrowsOnEmptyString() {
		assertThrows(RuntimeException.class, () -> parser.map(""));
	}
	
	@Test
	public void testThrowsOnOutOfRangeIndex() throws Exception {
		assertThrows(RuntimeException.class, () -> parser.map("-1 0.0 0.0"));
	}
	
	@Test
	public void testThrowsOnInvalidIndex() {
		assertThrows(NumberFormatException.class, () -> parser.map("invalid 0.0 0.0"));
	}
	
	@Test
	public void testThrowsOnInvalidLongitude() {
		assertThrows(NumberFormatException.class, () -> parser.map("0 invalid 0.0"));
	}
	
	@Test
	public void testThrowsOnInvalidLatitude() {
		assertThrows(NumberFormatException.class, () -> parser.map("0 0.0 invalid"));
	}
	
}
