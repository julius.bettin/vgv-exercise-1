package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DistanceAdderTest {
	private final DistanceAdder adder = new DistanceAdder();
	
	@Test
	public void testThatItReallyAdds() throws Exception {
		double result = adder.reduce(42.0, 13.37);
		assertEquals(55.37, result, 0.00001);
	}
}
