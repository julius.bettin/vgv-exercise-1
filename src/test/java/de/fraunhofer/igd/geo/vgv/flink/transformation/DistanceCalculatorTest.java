package de.fraunhofer.igd.geo.vgv.flink.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.java.tuple.Tuple2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.fraunhofer.igd.geo.vgv.flink.Position;
import de.fraunhofer.igd.geo.vgv.flink.util.MockCollector;

public class DistanceCalculatorTest {

	private final DistanceCalculator calculator = new DistanceCalculator();
	private MockCollector<Double> collector;
	
	@BeforeEach
	public void setup() {
		collector = new MockCollector<>();
	}
	
	@Test
	public void testCalculatesCorrectDistance() throws Exception {
		List<Tuple2<Long, Position>> records = new ArrayList<>();
		records.add(new Tuple2<Long, Position>(0L, new Position(42, 0)));
		records.add(new Tuple2<Long, Position>(0L, new Position(0, 42)));
		calculator.reduce(records, collector);
		assertEquals(1, collector.getRecordCount());
		assertEquals(6280, collector.getRecord(0), 0.1);
	}
	
}
