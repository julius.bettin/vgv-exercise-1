package de.fraunhofer.igd.geo.vgv.flink;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PositionTest {

	@Test
	public void testCalculatesCorrectDistance() {
		double distance = new Position(0, 42).distanceTo(new Position(42, 0));
		System.out.println(distance);
		assertEquals(6280, distance, 0.1);
	}
	
	@Test
	public void testDistanceIsCommutative() {
		double distance1 = new Position(0, 42).distanceTo(new Position(42, 0));
		double distance2 = new Position(42, 0).distanceTo(new Position(0, 42));
		assertEquals(distance1, distance2, 0.00001);
	}
	
}
