package de.fraunhofer.igd.geo.vgv.flink;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.utils.ParameterTool;

import de.fraunhofer.igd.geo.vgv.flink.transformation.PositionParser;
import de.fraunhofer.igd.geo.vgv.flink.transformation.PositionSplitter;
import de.fraunhofer.igd.geo.vgv.flink.transformation.DistanceAdder;
import de.fraunhofer.igd.geo.vgv.flink.transformation.DistanceCalculator;

public class BorderGermany {

    public static void main(String... args) throws Exception {

        // Checking input parameters
        final ParameterTool params = ParameterTool.fromArgs(args);
        String dataFile = ""; // Path to the file with all geo locations.
        if (params.has("data")) {
            dataFile = params.get("data");
        } else {
            throw new RuntimeException("No --data attribute passed. Set the location of the input data file.");
        }

        // set up execution environment
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        
        DataSource<String> dataSource = env.readTextFile(dataFile);
        long lineCount = dataSource.count();
        
        Double result = dataSource
        		// 1. parse line into tuple consisting of line index and position
        		.map(new PositionParser())
        		// 2. duplicate tuple, assigning a group index based on the line index
        		.flatMap(new PositionSplitter(lineCount))
        		// 3. group by group index, yielding pairs of positions
        		.groupBy(0)
        		// 4. calculate distance between position pairs
        		.reduceGroup(new DistanceCalculator())
        		// 5. sum distances
        		.reduce(new DistanceAdder())
        		// 6. retrieve result
        		.collect().get(0);
        
        System.out.println(String.format("Done: Distance = %f km", result));
    }

}
