package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import de.fraunhofer.igd.geo.vgv.flink.Position;

public class PositionParser implements MapFunction<String, Tuple2<Long, Position>> {

	private static final long serialVersionUID = -6348366251220313353L;

	@Override
	public Tuple2<Long, Position> map(String value) throws Exception {
		String[] parts = value.split(" ");
		if (parts.length != 3) {
			throw new RuntimeException(String.format("Invalid position format: '%s'", value));
		}
		long index = Long.parseLong(parts[0]);
		if (index < 0) {
			throw new RuntimeException(String.format("Invalid position index: '%d'", index));
		}
		return new Tuple2<>(index, new Position(Double.parseDouble(parts[1]), Double.parseDouble(parts[2])));
	}

}
