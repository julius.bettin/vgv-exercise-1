package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.ReduceFunction;

public class DistanceAdder implements ReduceFunction<Double> {

	private static final long serialVersionUID = -3447484371764385013L;

	@Override
	public Double reduce(Double value1, Double value2) throws Exception {
		return value1 + value2;
	}

}
