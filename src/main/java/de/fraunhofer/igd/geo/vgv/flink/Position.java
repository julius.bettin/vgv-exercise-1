package de.fraunhofer.igd.geo.vgv.flink;

public class Position {

	private static final double EARTH_RADIUS = 6371;
	
    private double longitude;
	private double latitude;

    public Position(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

	public double getLongitude() {
		return longitude;
	}

    public double getLatitude() {
		return latitude;
	}

    /**
     * Calculates the distance from this position to the passed position.
     * @param other Another position.
     * @return The distance in kilometers.
     */
    public double distanceTo(Position other) {
        double latitudeDistance = Math.toRadians(other.latitude - latitude);
        double longitudeDistance = Math.toRadians(other.longitude - longitude);
        double a = Math.sin(latitudeDistance / 2) * Math.sin(latitudeDistance / 2)
	        + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(other.latitude))
	        * Math.sin(longitudeDistance / 2) * Math.sin(longitudeDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c;
    }
}