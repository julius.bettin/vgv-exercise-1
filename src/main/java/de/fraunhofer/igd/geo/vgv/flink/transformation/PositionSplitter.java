package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

import de.fraunhofer.igd.geo.vgv.flink.Position;

public class PositionSplitter implements FlatMapFunction<Tuple2<Long, Position>, Tuple2<Long, Position>> {

	private static final long serialVersionUID = 1270688019735659182L;
	
	private long totalCount;
	
	public PositionSplitter(long totalCount) {
		this.totalCount = totalCount;
	}

	@Override
	public void flatMap(Tuple2<Long, Position> value, Collector<Tuple2<Long, Position>> out) throws Exception {
		/*
		 * we need at least 2 positions to be able to calculate a distance,
		 * so we duplicate each position and assign a different group index
		 * to the second one:
		 * 
		 * (0, p0)     (0, p0) (0, p4)
		 * (1, p1)     (1, p1) (1, p0)
		 * (2, p2) ==> (2, p2) (2, p1)
		 * (3, p3)     (3, p3) (3, p2)
		 * (4, p4)     (4, p4) (4, p3)
		 */
		long groupNumber1 = value.f0; // same as line index
		long groupNumber2 = (groupNumber1 + 1) % totalCount; // same group index as next element (with wrap-around)
		out.collect(new Tuple2<>(groupNumber1, value.f1));
		out.collect(new Tuple2<>(groupNumber2, value.f1));
	}

}
