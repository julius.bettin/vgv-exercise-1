package de.fraunhofer.igd.geo.vgv.flink.transformation;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

import de.fraunhofer.igd.geo.vgv.flink.Position;

public class DistanceCalculator implements GroupReduceFunction<Tuple2<Long, Position>, Double> {

	private static final long serialVersionUID = -3920115763478428510L;

	@Override
	public void reduce(Iterable<Tuple2<Long, Position>> values, Collector<Double> out) throws Exception {
		Tuple2<Long, Position> current = null;
		double distance = 0;
		for (Tuple2<Long, Position> value : values) {
			if (current != null) {
				distance += current.f1.distanceTo(value.f1);
			}
			current = value;
		}
		out.collect(distance);
	}

}
